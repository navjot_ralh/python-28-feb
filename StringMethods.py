# ab='Hello this is a string'
# ab=ab.center(100,'@')
# ab=ab.replace('s','S')
# list=ab.split()
# list=ab.split('i')
# print(list)
# print(ab)
# print(ab[0])
# print(list[1])
# print(len(list))
# print(len(ab))

# ab='    Hello There     '
# intro='''python is a general programming language
# python is easy to understand python is interpreter based language'''
# ab=ab.strip()                         # removes spaces
# ab=ab.rstrip()                        # removes spaces from right side
# ab=ab.lstrip()                        # removes spaces from left side
# ab=ab.count('o')
# print(ab,'next')
# print(ab)
# intro=intro.count('python')
# intro=intro.count('z')                # counts characters which are present in string otherwise produces error
# print(intro)

ab='this is a string in python'
cd='hello999'
# ab=ab.index('z')
# ab=ab.find('i')
# ab=ab.find('i',3,10)
# ab=ab.find('i',6,len(ab))

# ab=ab.isalpha()                         # shows TRUE if the string has no space or number nd FALSE if it contains spaces and numbers
# cd=cd.isalpha()                         
# ab=cd.isalnum()                           # shows TRUE when the string contains numbers and characters

# ab=ab.upper()
# ab=ab.lower()
ab=ab.capitalize()
print(ab)
# print(cd)